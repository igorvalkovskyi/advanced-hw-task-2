const navigationIcon = document.querySelector(".material-symbols-outlined");
        const navigationList = document.querySelector(".navigation__list");
        const mainContent = document.querySelector(".main__content_div_flowers");
        const mainInfoContent = document.querySelector(".info_comments");
        const contentCafe = document.querySelector(".info_comments_cafe");
        const contentWalks = document.querySelector(".info_comments_walks");
        const contentFlowers = document.querySelector(".info_comments_flowers");
        const footerSpan = document.querySelector(".footer__span");
        const pictureCar = document.querySelector(".instagram-shots-img-car");
        const linkInstPage = document.querySelector(".main__link_instagram_page");
        const pictureDoor = document.querySelector(".instagram-shots-img-door");
        const pictureWires = document.querySelector(".instagram-shots-img-wires");
        navigationIcon.addEventListener("click", function(){
            if(navigationIcon.innerText === "menu"){
                navigationIcon.innerText = "close";
            }else {
                navigationIcon.innerText = "menu"
            }
            navigationList.classList.toggle("invisible");
        })

        if(window.matchMedia("(min-width: 768px)").matches){
            navigationList.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 768px)").matches){
            mainContent.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 768px)").matches){
            mainInfoContent.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 768px)").matches){
            contentCafe.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 768px)").matches){
            contentWalks.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 768px)").matches){
            contentFlowers.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 768px)").matches){
            footerSpan.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 1200px)").matches){
            pictureCar.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 768px)").matches){
            linkInstPage.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 768px)").matches){
            pictureDoor.classList.remove("invisible");
        }
        if(window.matchMedia("(min-width: 768px)").matches){
            pictureWires.classList.remove("invisible");
        }