const gulp = require("gulp");
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const gulpClean = require('gulp-clean');
const cssMin = require('gulp-cssmin');
const rename = require('gulp-rename');
const gulpAutoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const gulpMinify = require('gulp-minify');
const gulpImagemin = require('gulp-imagemin');

const concatCss = function() {
    return gulp.src("dist/css/*.css")
    .pipe(concat("all.css"))
    .pipe(gulp.dest("./dist/css/"));
}

const clean = function(){
    return gulp.src("dist")
    .pipe (gulpClean());
}

const minCss = function() {
    return gulp.src("dist/css/*.css")
    .pipe(concat("all.css"))
    .pipe(cssMin())
    .pipe(rename({suffix:".min"}))
    .pipe(gulp.dest("./src/css/"));
}

const autoprefixer = function() {
    return gulp.src("all.css")
    .pipe(gulpAutoprefixer({cascade: false}))
    .pipe(gulp.dest("dist"));
}

const cssClean = function() {
    return gulp.src("dist/css/*.css")
    .pipe(cleanCSS({compatibility: "ie8"}))
    .pipe(gulp.dest("dist"));
}

const minify = function() {
    return gulp.src("src/js/*.js")
    .pipe(gulpMinify())
    .pipe(gulp.dest("src"));
}

const moveMinCssJs = function() {
    return gulp.src("src/js/*min.js","src/css/*min.css")
    .pipe(gulp.dest("dist/css/"));
}

const imagemin = function() {
    return gulp.src("src/img/*")
    .pipe(gulpImagemin())
    .pipe(gulp.dest("dist/img"));
}

const build = function() {
    return gulp.series(clean,style,autoprefixer,cssClean,minify,moveMinCssJs,imagemin);
}

const dev = function() {
    return gulp.series(build,watch);
}

gulp.task("concatCss", concatCss);
gulp.task("clean", clean);
gulp.task("minCss", minCss);
gulp.task("autoprefixer", autoprefixer);
gulp.task("cssClean", cssClean);
gulp.task("minify", minify);
gulp.task("moveMinCssJs", moveMinCssJs);
gulp.task("imagemin", imagemin);
gulp.task("build", build);
gulp.task("dev", dev);

function style() {
    return gulp.src("src/scss/**.scss")
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(gulp.dest("dist/css"));
}

function watch() {
    gulp.watch("src/scss/**.scss", style)
}

exports.style = style;
exports.watch = watch;



